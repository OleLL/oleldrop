from flask import Blueprint
from flask import render_template
from flask import request
from flask import abort
from flask import send_from_directory, send_file

from werkzeug.utils import secure_filename
import os
import io
import hashlib

import config
from oleldrop.user import get_logged_in_user
from oleldrop.util import crypto
from oleldrop.admin.models import Item

download_page = Blueprint('donwload', __name__)

@download_page.route("/download/<share_key>", methods=["GET", "POST"])
def download(share_key):
    items = Item.select().where(Item.share_key==share_key)
    if len(items) > 0:
        item = items[0]
        if not item.is_encrypted:
            return send_from_directory(config.DROP_FILES_LOCATION, item.filename.split("/")[-1], as_attachment=True)
        else:
            if "key" in list(request.args.keys()):
                key = request.args["key"]
            elif "key" in list(request.form.keys()):
                key = request.form["key"]
            else:
                backref = "http{s}://{host}/download/{share_key}".format(s='s' if config.HTTPS else '',
                                                                         host=config.HOST, share_key=share_key)
                return render_template("key_input.html", config=config,
                										 backref=backref,
                										 https="s" if config.HTTPS else "")
            key = hashlib.md5(key.encode("utf-8")).digest()
            dec = crypto.decrypt_file(item.filename, key)
            buf = io.BytesIO()
            buf.write(dec)
            buf.seek(0)
            return send_file(buf, as_attachment=True,
                     attachment_filename=item.filename.split("/")[-1])
    else:
        return abort(404)


@download_page.route("/share/<share_key>", methods=["GET", "POST"])
def share(share_key):
    items = Item.select().where(Item.share_key==share_key)
    if len(items) > 0:
        item = items[0]
        if not item.is_encrypted:
            return send_from_directory(config.DROP_FILES_LOCATION, item.filename.split("/")[-1], as_attachment=False, mimetype=item.mime_type)
        else:
            if "key" in list(request.args.keys()):
                key = request.args["key"]
            elif "key" in list(request.form.keys()):
                key = request.form["key"]
            else:
                backref = "http{s}://{host}/share/{share_key}".format(s='s' if config.HTTPS else '',
                                                                         host=config.HOST, share_key=share_key)
                return render_template("key_input.html", config=config,
                										 backref=backref,
                										 https="s" if config.HTTPS else "")
            key = hashlib.md5(key.encode("utf-8")).digest()
            dec = crypto.decrypt_file(item.filename, key)
            buf = io.BytesIO()
            buf.write(dec)
            buf.seek(0)
            return send_file(buf, as_attachment=False, attachment_filename=item.filename.split("/")[-1])
    else:
        return abort(404)