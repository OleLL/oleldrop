from oleldrop.admin.models import Item
import subprocess

def get_mime_type(filename):
	return subprocess.check_output(["file", "--mime", filename]).decode("utf-8").split(":")[1][1:].split(";")[0]

def create_item(filename, encrypted, description):
	mime_type = get_mime_type(filename)
	item = Item(filename=filename, is_encrypted=encrypted, description=description, mime_type=mime_type)
	item.update_share_key()
	item.save()
	return item