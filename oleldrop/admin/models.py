import peewee
import random
import string
import datetime

from oleldrop.__main__ import database as main_db
from oleldrop.const import constants

class Item(peewee.Model):

    filename = peewee.CharField()
    is_encrypted = peewee.BooleanField(default=False)
    mime_type = peewee.CharField()
    description = peewee.CharField()
    uploaded = peewee.DateTimeField(default=datetime.datetime.now)
    share_key = peewee.CharField()

    def update_share_key(self):
        charset = string.ascii_letters + string.digits
        self.share_key = ''.join([charset[random.randint(0, len(charset) - 1)] for _ in range(0, 12)])

    class Meta:
        database = main_db
