from flask import Blueprint
from flask import render_template
from flask import request
from flask import redirect

from werkzeug.utils import secure_filename
import os

import config
from oleldrop.user import get_logged_in_user
from oleldrop.util import crypto
from oleldrop.admin import create_item
from oleldrop.admin.models import Item
import hashlib
import cv2

admin_page = Blueprint('admin', __name__)

@admin_page.route("/admin")
def admin():
    user = get_logged_in_user()
    if user:
        return render_template("admin.html")
    else:
        return redirect("/", 302)

@admin_page.route("/add_by_upload", methods=["POST", "GET"])
def add_by_upload():
	user = get_logged_in_user()
	if user:
		print(request.method)
		if request.method == "GET":
			return render_template("add_by_upload.html", config=config)
		elif request.method == "POST":
			keys = list(request.form.keys())
			if "description" in keys:
				if "encrypt" in keys and not "key" in keys:
					return redirect("/admin", 302)
				file = request.files['file']
				if file:
					if file.filename != "":
						filename = secure_filename(file.filename)
						path = os.path.join(config.DROP_FILES_LOCATION, filename)
						file.save(path)
						if "encrypt" in keys:
							key = hashlib.md5(request.form["key"].encode("utf-8")).digest()
							crypto.encrypt_file(path, key)
						item = create_item(path, "encrypt" in keys, request.form["description"])
						if item:
							if not item.is_encrypted:
								if item.mime_type.split("/")[0] == "image":
									im = cv2.imread(item.filename)
									im = cv2.resize(im, (200, 200))
									cv2.imwrite(config.THUMB_LOCATION + item.share_key + ".png", im)
								if item.mime_type.split("/")[0] == "video":
									cap = cv2.VideoCapture(item.filename)
									ret = False
									while not ret:
										ret, im = cap.read()
									cap.release()
									im = cv2.resize(im, (200, 200))
									cv2.imwrite(config.THUMB_LOCATION + item.share_key + ".png", im)
			return redirect("/admin", 302)
	return redirect("/", 302)

@admin_page.route("/edit/<share_key>")
def edit(share_key):
    user = get_logged_in_user()
    if (user or config.DROP_CONTENT_PUBLIC):
        items = Item.select().where(Item.share_key==share_key)
        if len(items) > 0:
            item = items[0]
            return render_template("edit_item.html",config=config,
                                             description=item.description,
                                             mime_type=item.mime_type,
                                             encryption="Yes" if item.is_encrypted else "No",
                                             thumb="/thumb/{share_key}.png".format(share_key=item.share_key),
                                             https="s" if config.HTTPS else "",
                                             share_key=item.share_key)
        else:
            return redirect("/", 302)
    else:
        return redirect("/", 302)

@admin_page.route("/update", methods=["POST"])
def update():
    user = get_logged_in_user()
    if user:
        keys = list(request.form.keys())
        if "share_key" in keys and "new_share_key" in keys and "new_description" in keys:
            items = Item.select().where(Item.share_key==request.form["share_key"])
            if len(items) > 0:
                items[0].share_key = request.form["new_share_key"]
                items[0].description = request.form["new_description"]
                items[0].save()
                try:
                    os.rename(config.THUMB_LOCATION + request.form["share_key"] + ".png",
                              config.THUMB_LOCATION + request.form["new_share_key"] + ".png")
                except:
                    pass
                return redirect("/", 302)
        else:
            return redirect("/", 302)
    else:
        return redirect("/", 302)

@admin_page.route("/delete", methods=["POST"])
def delete():
    user = get_logged_in_user()
    if user:
        keys = list(request.form.keys())
        if "share_key" in keys:
            if "delete" in keys:
                delete_on_disk = True
            else:
                delete_on_disk = False
            items = Item.select().where(Item.share_key==request.form["share_key"])
            if len(items) > 0:
                path = items[0].filename
                if delete_on_disk:
                    try:
                        os.remove(path)
                    except:
                        pass
                items[0].delete_instance(recursive=True)
                return redirect("/", 302)
        else:
            return redirect("/", 302)
    else:
        return redirect("/", 302)