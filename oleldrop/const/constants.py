"""
In this file some common constants are defined.
"""

# Password related stuff
from hashlib import sha512
PASSWORD_HASH_CYCLES = 100
PASSWORD_HASH_TYPE = sha512
PASSWORD_SALT_LENGTH = 32
