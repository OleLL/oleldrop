VERSION_STRING = "1.0"
VERSION_NAME = "Milotic"

def get_printable_version():
	return "{version_string} ({version_name})".format(version_string=VERSION_STRING,
		                                              version_name=VERSION_NAME)