from flask import Flask
from flask import send_file
from flask import send_from_directory
from flask import abort
from flask import render_template
import peewee
import os

database = peewee.SqliteDatabase('oleldrop.sqlite3')
database.connect()

import config

from oleldrop.util import lellog
from oleldrop.const.version import get_printable_version
from oleldrop.admin.views import admin_page
from oleldrop.user.views import user_page
from oleldrop.index.views import index_page
from oleldrop.download.views import download_page
from oleldrop.user.models import User, Password, Session
from oleldrop.admin.models import Item
from oleldrop.util.generate_theme import get_random_theme
database.create_tables([User, Password, Session, Item])

app = Flask(__name__,  template_folder=config.TEMPLATE_LOCATION)
app.register_blueprint(admin_page)
app.register_blueprint(user_page)
app.register_blueprint(index_page)
app.register_blueprint(download_page)

with open(config.TEMPLATE_LOCATION + "index.css") as target:
	css_string = target.read()

# TODO: This might be a security risk and should be done by another webserver like nginx
@app.route('/static/<filename>')
def static_(filename):
    if filename.split(".")[-1] not in ["html", "css"]:
        return send_from_directory(config.STATIC_LOCATION, filename, as_attachment=True)
    else:
        return send_from_directory(config.STATIC_LOCATION, filename)
@app.route('/thumb/<path>')
def thumb_(path):
    if path in os.listdir(config.THUMB_LOCATION):
        return send_file(config.THUMB_LOCATION + path)
    else:
    	return send_file(config.STATIC_LOCATION + "/no_prev.png")

@app.route("/about")
def about_():
	return render_template("about.html", config=config)

@app.route("/index.css")
def index_css():
	theme = get_random_theme()
	return css_string.replace("^background^", theme["background"]).replace("^foreground^", theme["foreground"]).replace("^darker^", theme["darker"]).replace("^header^", theme["header"])

lellog.info("Welcome to oleldrop - This is version %s." % get_printable_version())
