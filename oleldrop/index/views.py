from flask import Blueprint
from flask import render_template

import config
from oleldrop.user import get_logged_in_user
from oleldrop.admin.models import Item

index_page = Blueprint('index', __name__)

@index_page.route("/")
def index():
	user = get_logged_in_user()

	item_html = ""
	if user or config.DROP_CONTENT_PUBLIC:
		items = Item.select()
		if len(items) > 0:
			for i in range(0, len(items))[::-1]:
				item = items[i]
				item_html += render_template("single_item.html", href="/edit/" + item.share_key,
					                                             iid=i,
					                                             thumb_ref="/thumb/{share_key}.png".format(share_key=item.share_key),
					                                             description=item.description,
					                                             encryption="Yes" if item.is_encrypted else "No",
					                                             host=config.HOST,
					                                             mime_type=item.mime_type,
					                                             share_key=item.share_key)
		else:
			item_html = "This drop seems to be empty."
	else:
		item_html = "The content of this drop isn't public"

	return render_template("index.html",
		                   config=config,
		                   login_str="Hello {username}".format(username=user.name) if user else "Login", 
		                   login_ref="#" if user else "/login",
		                   item_html=item_html)