import colorsys
import random

class Color(object):

	def __init__(self, r, g, b, a=255):
		self.r = r
		self.g = g
		self.b = b
		self.a = a

		self.h, self.s, self.v = colorsys.rgb_to_hsv(self.r, self.g, self.b)

	def html(self):
		return "#%02x%02x%02x%02x" % (self.r, self.g, self.b, self.a)

	def rgb(self):
		return self.r, self.g, self.b

	def rgba(self):
		return self.r, self.g, self.b, self.a

	def hsv_move(self, h=0, s=0, v=0):
		r, g, b = map(int, colorsys.hsv_to_rgb((self.h+h)%1.001, (self.s+s)%1.001, (self.v+v)%256))
		return Color(r, g, b, self.a)

	def copy(self):
		return Color(self.r, self.g, self.b, self.a)

def get_random_theme():
	background_h = random.randint(0, 1000) / 1000.
	background = Color(*map(int, colorsys.hsv_to_rgb(background_h, 1, 255)))
	background = background.hsv_move(s=-0.6, v=-60)
	foreground = background.hsv_move(h=-0.06, v=-20)
	darker = background.hsv_move(v=-70)
	header = darker.copy()
	header.a = 150
	return {
		"background": background.html(),
		"foreground": foreground.html(),
		"darker": darker.html(),
		"header": header.html()
	}

if __name__ == "__main__":
	print(get_random_theme())