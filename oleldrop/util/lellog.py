import time
import os

# Log levels:
# 4 -> Everything: Debug, Info, Warning, Error, Fatal
# 3 -> Info, Warning, Error, Fatal
# 2 -> Warning, Error, Fatal
# 1 -> Error, Fatal
# Fatal is on every log level visible
DEFAULT_LOG_LEVEL = 4
log_level = DEFAULT_LOG_LEVEL


def refresh_log_level():
    pass  # TODO: This method should refresh the log level


def set_log_level(level):
    global log_level
    log_level = level


refresh_log_level()


class EscapeSequences(object):
    # Foreground colors
    red = '\033[31m'
    green = '\033[32m'
    yellow = '\033[33m'
    blue = '\033[34m'
    purple = '\033[35m'
    cyan = '\033[36m'
    gray = '\033[37m'

    # Background colors
    bg_red = '\033[41m'
    bg_green = '\033[42m'
    bg_yellow = '\033[43m'
    bg_blue = '\033[44m'
    bg_purple = '\033[45m'
    bg_cyan = '\033[46m'
    bg_gray = '\033[47m'

    reset = '\033[0m'  # Reset all colors, etc.
    bold = '\033[1m'
    underscore = '\033[4m'
    blink = '\033[5m'  # turn blinking text on
    reverse = '\033[7m'  # Reverse background and foreground color
    conceal = '\033[8m'

    cursor = lambda row, column: '\033[%d;%dH' % (row, column)
    cursor_up = lambda rows: '\033[%dA' % rows
    cursor_down = lambda rows: '\033[%dB' % rows
    cursor_backward = lambda cols: '\033[%dD' % cols
    cursor_forward = lambda cols: '\033[%dC' % cols
    save_cursor = '\033[s'
    restore_cursor = '\033[u'

    clear_screen = '\033[2J'
    clear_line = '\033[K'


def _log(type_text, type_color, message_text, message_color):
    """
    This method generates a fixed length and colored string from your input
    and prints it
    DO NOT USE THIS METHOD FOR LOGGING, USE DEBUG, WARNING, INFO, etc...
    """
    time_string = "%s%s%s" % (EscapeSequences.bold, time.strftime("%H:%M:%S"), EscapeSequences.reset)
    type_string = "%s%s%s" % (type_color, type_text, EscapeSequences.reset)
    uncolored_time_string = time_string

    message_string = "%s%s%s" % (message_color, message_text, EscapeSequences.reset)
    log_string = "%s~%s (%s) [%s] " % (EscapeSequences.purple, EscapeSequences.reset, time_string, type_string)
    uncolored_log_string = "~ (%s) [%s] " % (time_string, type_text)
    while len(uncolored_log_string) < 24:  # len("~ (WARNING) [13:37:42]  ") == 24
        log_string += " "
        uncolored_log_string += " "

    print_string = log_string + message_string
    print_lines = print_string.split("\n")

    print(print_lines[0])
    for line in print_lines[1:]:
        if line is not None:
            print((" " * 22) + str(line))


def debug(message):
    """
    Log a debug message, debug messages are only on log level 4 visible
    """
    refresh_log_level()
    if log_level >= 4:
        _log("DEBUG", EscapeSequences.cyan, message, EscapeSequences.reset)


def info(message):
    """
    Log a info message, info messages are only on log level 3 and higher visible
    """
    refresh_log_level()
    if log_level >= 3:
        _log("INFO", EscapeSequences.blue, message, EscapeSequences.reset)


def warning(message):
    """
    Log a warning message, warning messages are only on log level 2 and hihger visible
    """
    refresh_log_level()
    if log_level >= 2:
        _log("WARNING", EscapeSequences.yellow, message, EscapeSequences.reset)


def error(message):
    """
    Log a error message, error messages are only on log level 1 or higher visible
    """
    refresh_log_level()
    if log_level >= 1:
        _log("ERROR", EscapeSequences.red, message, EscapeSequences.reset)


def fatal(message):
    """
    Log a fatal message, fatal messages are on all log levels visible
    """
    _log("FATAL", EscapeSequences.blink + EscapeSequences.red + EscapeSequences.reverse + EscapeSequences.bold, message,
         EscapeSequences.red)


if __name__ == "__main__":
    set_log_level(5)

    fatal("This is a fatal message")
    error("This is an error message")
    warning("This is a warning")
    info("This is a info")
    debug("This is a debug message")
