from flask import Blueprint
from flask import request
from flask import render_template
from flask import redirect

from oleldrop.user import login_user, get_logged_in_user
from oleldrop.user.models import User, Session

import config

user_page = Blueprint('user', __name__)

@user_page.route("/logout")
def logout():
	user = get_logged_in_user()
	if user:
		Session.delete().where(Session.user==user)
	return redirect("/", 302)


@user_page.route("/login", methods=["GET", "POST"])
def login_page():
	if request.method == "GET":
		return render_template("login.html", config=config, message="Please log in.")

	elif request.method == "POST":
		keys = list(request.form.keys())
		if "username" in keys and "password" in keys:
			ret = login_user(request.form["username"], request.form["password"])
			if ret:
				user, session = ret
				response = redirect("/", 302)
				response.set_cookie("session_token", session.token)
				return response
			else:
				return render_template("login.html", config=config, message="Invalid credentials, please try again.")
		else:
			return render_template("login.html", config=config, message="Missing username and/or password in post data.")