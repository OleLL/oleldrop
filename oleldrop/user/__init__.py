from oleldrop.user.models import User, Session, Password
from flask import request

def create_user(username, password):
    user = User(name=username)
    user.set_password(password)
    user.save()
    return user

def login_user(username, password):
    try:
        user = User.get(name=username)
    except:
        return False
    if user.password.verify(password):
        session = user.create_session()
        return user, session
    else:
        return False

def get_logged_in_user():
    if "session_token" in list(request.cookies.keys()):
        return authenticate_user_token(request.cookies["session_token"])

def authenticate_user_token(token, username=None):
    try:
        session = Session.get(token=token)
    except:
        return 
        
    if username:
        try:
            user = User.get(name=username)
        except:
            return 

        if session.user == user:
            return user
        else:
            return 
    else:
        return session.user
