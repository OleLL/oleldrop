import peewee
import hashlib
import random

from oleldrop.__main__ import database as main_db
from oleldrop.const import constants

class Password(peewee.Model):
    password_id = peewee.PrimaryKeyField()
    password = peewee.CharField(max_length=constants.PASSWORD_HASH_TYPE().digest_size * 2)
    salt = peewee.CharField(max_length=constants.PASSWORD_SALT_LENGTH)

    def set_value(self, new_value):
        # Generating the salt by creating n/2 random numbers and converting them to hex
        self.salt = ''.join(['%02x' % random.randint(0, 256) for _ in range(0, int(constants.PASSWORD_SALT_LENGTH/2))])
        calculated_hash = self.get_cycled_hash(new_value + self.salt)
        self.password = calculated_hash

    def verify(self, password):
        calculated_hash = self.get_cycled_hash(password + self.salt)
        return self.password == calculated_hash

    def get_cycled_hash(self, text):
        calculated_hash = text
        hash_ = constants.PASSWORD_HASH_TYPE()
        for i in range(0, constants.PASSWORD_HASH_CYCLES):
            hash_.update(calculated_hash.encode("utf-8"))
            calculated_hash = hash_.hexdigest()
        return calculated_hash

    class Meta:
        database = main_db

class User(peewee.Model):
    user_id = peewee.PrimaryKeyField()
    name = peewee.CharField()
    password = peewee.ForeignKeyField(Password, to_field="password_id")

    def set_password(self, password):
        password_ = Password()
        password_.set_value(password)
        password_.save()
        self.password = password_

    def create_session(self):
        session = Session()
        session.user = self
        session.save()
        return session

    class Meta:
        database = main_db


class Session(peewee.Model):

    def __init__(self, *args, **kwargs):
        super(Session, self).__init__(*args, **kwargs)

        _token = ''.join(['%02x' % random.randint(0, 256) for _ in range(0, 256)])
        hash = hashlib.sha512()
        hash.update(_token.encode("utf-8"))
        self.token = hash.hexdigest()

        self.is_active = True


    is_active = peewee.BooleanField()
    token = peewee.CharField(max_length=256)

    user = peewee.ForeignKeyField(User, backref="sessions", to_field="user_id")

    class Meta:
        database = main_db
