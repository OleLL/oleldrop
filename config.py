DROP_NAME = "oleldrop example config"
DROP_DESCRIPTION = "oleldrop example config"

DROP_CONTENT_PUBLIC = False
DROP_LOCATION = "/"

DROP_FILES_LOCATION = DROP_LOCATION + "upload/"
THUMB_LOCATION = DROP_LOCATION + "thumb/"
TEMPLATE_LOCATION = DROP_LOCATION + "templates/"
STATIC_LOCATION = DROP_LOCATION + "static/"

HTTPS = False
HOST = "localhost:5000"
